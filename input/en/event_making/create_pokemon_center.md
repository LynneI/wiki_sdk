# Create a Pokémon Center

![tip](warning "This page comes from the **old Wiki**. It's probably not up to date.")

In this tutorial we'll only see the Event Making part of the Pokémon Center since the mapping is a basis.

## The first event : The nurse

![Infirmière|right](img/event_making/Psdk_centre_01.png "Nurse event")
In a Pokémon Center under PSDK the Nurse Joy is very important. When you blacked out and PSDK send you back to the Pokémon Center, it has to find the Nurse in order to heal you.

The easiest way to do that is to make sure the Nurse is the first event : Event 001.

This event is pretty simple, it calls the common event `4` "**Centre - Infirmière**".

## The healing machines

![Machine Gauche Page 1|left](img/event_making/Psdk_centre_02.png "Left machine page 1")
![Machine Droite Page 1|right](img/event_making/Psdk_centre_03.png "Right machine page 1")
In the Pokemon Center there's one Healing machine but since it takes two tiles, we use two events. The healing common event trigger the switch that helps the healing machine display the animation.

![Machine Gauche Page 2|left](img/event_making/Psdk_centre_04.png "Left machine page 2")
![Machine Droite Page 2|right](img/event_making/Psdk_centre_05.png "Right machine page 2")

## The Pokémon Storage System

![PC|right](img/event_making/Psdk_centre_06.png "Storage computer")
The Pokemon Storage System is pretty simple, you just have to call a script command in the corresponding event :

## The exit event

The exit event is pretty important, it tells to PSDK where is the last Pokémon Center you visited. Here's how to do it:

![Sortie|center](img/event_making/Psdk_centre_07.png "Exit event")

![tip](info "In this event there's the 6th switch that is turned off. It allows you to enable the Map Connection outside of the Pokemon Center. You should always enable this switch **before** warping.")
