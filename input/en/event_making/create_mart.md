![Shop UI|center](img/shop_ui.png)

# Create a shop

For starters, it's important to note that the Shop will not accept selling items with a price inferior or equal to 0. Keep that in mind, it's really important for the rest of the tutorial.

There are two types of shops : those with unlimited stock and those with limited stock. This tutorial will teach you how to handle both.

## Open an unlimited shop

There are two ways to open an unlimited shop : 

### The simple but restricted way

The first and easiest way is to use the basic RMXP command. You create an event in which you call the "Open a Shop" command. Next, you add each item you want to see in your shop one at a time, you validate, and it's done.

![tip](warning "RMXP can't directly access the database of PSDK, so it will say the item costs 0. Don't worry about that, the real price will be the one you set in the database through Ruby Host.")

![Shop RMXP|center](img/open_shop.png)

### The less easy but oh so much more powerful way

The second way, a tad less easy to use, requires that you use the `open_shop` script command and allows you to temporarily change the price of your items. This commands accepts one mandatory and two optional parameters. Let's have a look.

The `open_shop` script command obviously needs as first parameter a list of items to be sold, either as an ID Array or a Symbol (which we'll discuss later when we'll see the limited shops). The second parameter must be a Hash, to allow temporarily changing the price of some items. Finally, the third parameter is a Boolean (true ou false), and turns on or off the animated background of the interface of the shop.

Here's an example we'll analyze together :
```ruby
open_shop([4, 17, 18, 22], {4 => 150, 17 => 300, 22 => 400}, show_background: true)
```

The first parameter is [4, 17, 18, 22] and is required. It tells the shop it will sell : Poké Balls, Potions, Antidotes and Paralyze Heals. Easy, right?

The second parameter is {4 => 150, 17 => 300, 22 => 400} and is optional. It tells the shop some items have another price than usual. In this case, the Poké Balls price is reduced to 150, the Potions price rises to 300 and Paralyze Heals price rises to 400. Antidotes have their default price.

The third parameter is show_background: true and is just as optional. Its default value is true. As such, we could as well not have mentioned it in the command. It tells the shop it should use an animated background. True to enable, false to disable.

The syntax really is important. The first parameter is incased in [], the second in {}, and the third is "show_background: " followed by its boolean value.

![tip](warning "If you remember what I said earlier, the shop will not allow to sell an item with a price of 0 or lower. Let's say you want to have the Master Ball for sale, as its price is set to 0, you will absolutely need to change it with the second parameter when you create that shop.")

## Open a limited shop

### Creation of the stock of the shop

To open a limited shop, you'll first need to create a shop with the list of available items and quantities. You'll need to that end the script command `add_items_to_limited_shop`. It takes two mandatory parameters and two optional ones. Let's analyze this with an example.

```ruby
add_limited_shop(:shop_argenta, [4, 17, 18, 22], [5, 3, 2, 1], shop_rewrite: false)
```

The first parameter is :shop_argenta and is required. It gives a name to your shop, which will serve later as an id for another command.

![tip](info "You can call your shop whatever you like as long as you respect the syntax. You can call it :old_geezer_shop or :walmart as long as you keep the : in front of its name. However, choosing names easily remembered will prevent you from having to look up how you named it.")

The second parameter is [4, 17, 18, 22] and is also required. It tells the shop the available items are : Poké Balls, Potions, Antidotes and Paralyze Heals.

The third parameter, [5, 3, 2, 1], is optional. It tells the shop how many of each items are available, in the same order as the previous Array. Meaning we'll have : 5 Poké Balls, 3 Potions, 2 Antidotes et 1 Paralyze Heal. If the Array doesn't hold enough entries or there's no Array at all, the shop will have only 1 unit of each item by default.

The fourth parameter is shop_rewrite: false and is optional. It tells the command if it should overwrite an existing shop with the same name. In the example, if :shop_argenta already exists, true would overwrite the old shop, false would merely add the new items to the old shop.

![tip](warning "It's unwise to create the stock of your shop in the same event as your shop for an obvious reason : you would risk replenishing the stock of the shop you wanted to open. Be sure to create it in another event.")

### Replenish the stock of items in a shop

As said earlier, you could use the `add_limited_shop` script command to add items to your shop. Or you could use the ad hoc command, `add_items_to_limited_shop`. This command has the exact same first three parameters as `add_limited_shop`.

### Deplete the stock of an item / remove an item from a shop

To reduce the available stock of an item or to remove that item entirely, only one command : `remove_items_from_limited_shop`. 

This command accepts the same parameters as the command to add stock, except that the parameter about the amount to remove is optional. And if it's not used, then the item will be removed entirely from the shop. If it's used, it will remove the amount of items according to its position in the Array.

Example :
```ruby
remove_items_from_limited_shop(:shop_argenta, [4, 17, 18, 22], [10, 15, 20])
```
This command will remove 10 Poké Balls, 15 Potions, 20 Antidotes, and completely remove Paralyze Heals from sales because there was no 4th number in the 3rd parameter.

### Open the limited shop

Now that the stock of the shop has been created, it's time to open it. And you only need to use the aforementioned `open_shop` script command with a twist in the parameters.

If you remember, it was said that you could give a Symbol as first parameter for that command : that's the name you gave to your shop. You only need to use that Symbol as first parameter, the rest is the exact same thing as earlier.

### Custom messages when leaving the shop

The new Shop includes 5 different scénarios allowing you to create custom messages depending how the player left the Shop. To use this feature, you only need to create a condition using the value of game variable 26 TMP1. Here are its possible values and what they mean :
- -1 : The shop could not open (no more stock)
- 0 : The player left having bought nothing
- 1 : The player left having bought 1+ units of the same item (1 Hyper Ball or 50 Hyper Ball will lead here)
- 2 : The player left having bought different items
- 3 : The player left having bought everything the shop had to offer, leaving it now empty (which will cause the auto closing of this shop and scenario in -1 afterwards)
 
![Shop Scenario|center](img/shop_scenario_en.png)