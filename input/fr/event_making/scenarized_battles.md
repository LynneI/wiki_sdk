# Combats scénarisés

![tip](warning "Ce tutoriel est destiné à un public ayant un peu d’expérience dans le domaine du making car il requiert des connaissances sur les interrupteurs et sur de l’édition en général. Il ne faut donc pas avoir peur des commandes scriptées")

Les combats scénarisés sont toujours un point important à un moment ou un autre dans la réalisation du jeu pour différencier les dresseurs "normaux" des dresseurs de la ligue ou des rivaux.

Dans PSDK .24.XX, les combats scénarisés utilisent beaucoup l’onglet Troops RPG Maker XP mais pour le bon fonctionnement des combats, il faudra également quelque peu utiliser Ruby Host.

## Edition dans la Base de Données RubyHost (pour les dresseurs uniquement) :

Pour activer la scénarisation des combats, il faut se rendre dans l’onglet Dresseurs de RubyHost.
![tip] (info "Si vous avez oublié comment paramétrer un Dresseur, référez-vous à [la page correspondante] (trainer_event.html)")
Modifiez ensuite le numéro du groupe de combat (par défaut 0 qui appelle le ``Combat de Dresseur`` de l’onglet Troops dans RPG Maker XP) par un nombre supérieur à 0.
Sauvegardez, la partie d’édition sur RubyHost s’arrête ici.
![Fenêtre d'affichage des Dresseurs dans RubyHost](img/manage/Ruby_Host_trainers.png)

## Edition dans RPG Maker XP (Pokémon sauvages et Dresseurs) :

Une fois sur RPG Maker XP dans l’onglet Troops, vous avez le choix entre paramétrer un groupe de Pokémon sauvages et un Combat de Dresseur : 

### Paramétrer un groupe de Pokémon sauvages

Pour paramétrer un groupe de Pokémon sauvages scénarisé, il faut d’abord définir le groupe que vous souhaitez faire apparaître grâce à quelques commandes puisqu’il n’est pas possible d’éditer directement depuis RubyHost. 

![tip] (info "Il est entièrement possible de paramétrer des groupes aléatoires sur une zone donnée mais cela ne sera pas détaillé dans ce tutoriel car cela requiert des connaissances avancées en programmation.")

Pour cela rendez-vous dans l’onglet Troops de la Base de Données de RPG Maker XP : 
![Fenêtre de la Base de données de RPG Maker XP, onglet Troops](img/manage/RPGXP_Troops.png)

Créez un groupe que vous nommerez à votre convenance, ici, j’ai choisi ``Other test``.

Choisissez ensuite vos Pokémon dans la barre déroulante sur le côté en double-cliquant dessus puis créez le reste du groupe comme ci-dessus à savoir :

- Condition : ``Turn 0 `` 
- Span: ``Battle``
- Trois commandes scripts : 

```ruby
$scene.setup_battle(type, actor_count, enemy_count)
$scene.configure_pokemons(content)
$scene.select_pokemon(range, rareness)
```

Pour ces trois commandes,

- ``type`` correspond au type de combat (1 pour 1v1 et 2 pour 2v2)
- ``actor_count`` correspond au nombre de battlers alliés à afficher à la transition de combat (1 pour du 1v1)
- ``enemy_count`` correspond au nombres de battlers ennemis à afficher à la transition de combat (1 pour du 1v1)
- ``content`` peut correspondre soit à une suite de Hash (un hash s'écrit ``{:key => value}``) semblables à ``ajouter_pokemon_param`` soit à une suite de niveaux des Pokémon configurés

![tip](danger "Il ne peut pas y en avoir moins que de Pokémon, sinon vous risquez un crash")

- ``range`` correspond à l'écart de niveau par rapport aux niveaux définis au-dessus
- ``rareness`` correspond au taux de rencontre des Pokémon sauvages dans la zone


Une fois que vous avez ça, vous êtes parés pour utiliser les commandes relatives aux combats scénarisés. Pour appeler ce groupe sauvage, vous n'avez plus qu'à créer un évènement contenant ``Démarrer un combat : Votre Groupe de Sauvage``.

![tip](info "Remarque : Pour les Pokémon légendaires, vous pouvez appliquer la même méthode mais en ne mettant qu’un seul Pokémon")

### Paramétrer un Dresseur

Paramétrer un dresseur est très semblable à paramétrer un Pokémon si on choisit de tout faire à la main.
En effet, on réapplique la méthode précédente avec pour commandes scripts : 
```ruby
$scene.setup_battle(type, actor_count, enemy_count, battler_filename) #battler_filename correspond au fichier du dresseur
$scene.trainer_names("name") # name correspond au nom du dresseur
$scene.trainer_class(class_id) # class_id correspond à l'id de la classe des dresseurs définie dans les textes de RubyHost
$scene.trainer_phrases("Victory phrase", "Defeat phrase") # Phrases de victoires et de défaites
$scene.configure_pokemons(content) #Pareil qu'au-dessus
```
![Comment paramétrer un Dresseur](img/manage/RPGXP_Trainers.png)

Il existe toutefois une manière plus commode qui fonctionne avec RubyHost : 
Il s'agit de modifier le groupe de l'id ``Groupe de Combat`` en mettant pour ``Turn 0`` et ``Battle`` :
```ruby
$scene.init_trainer_battle
```
Cela peut sembler redondant avec le groupe ``Combat de Dresseur``, mais il n'en est rien car vous allez justement modifier des choses qui ne concernent pas les dresseurs classiques.

### Commandes
 Ce qu'il faut savoir, c'est que dans un évènement de combat, on peut utiliser la plupart des commandes classiques de RPG Maker XP. Les seules fonctionnalités absentes pour le moment sont les affichages d'images (C'est pour cela que pour les exemples ci-dessous, il n'y aura pas d'images).

 Cependant, pour savoir quand utiliser ces fonctions, il est impératif de savoir comment les combats sont agencés. Déjà dans le span, on peut voir qu'il y a plusieurs conditions selon le moment où l'on veut scénariser le combat : 
 
 - ``Battle`` : Une seule fois dans le combat
 - ``Turn`` : A chaque tour du combat
 - ``Moment`` : Démarre instantanément

 Ensuite il y a également les différents interrupteurs qui témoignent des différentes phases de combats : 

 - ``BT Mise à jour phase`` : Interrupteur activé à chaque changement de phase (voir ci-dessous les différentes phases)
 - ``BT Phase 1`` : Activé lors de la phase 1 : Transition d'entrée dans le combat
 - ``BT Phase 2`` : Activé lors de la phase 2 : Affichage du menu principal
 - ``BT Phase 3`` : Activé lors de la phase 3 : Sélection de la Cible
 - ``BT Phase 4`` : Activé lors de la phase 4 : Déroulement de l'action sélectionnée
 - ``BT Phase 5`` : Activé à la fin du combat
 - ``BT Victoire`` : Activé en cas de Victoire
 - ``BT Défaite`` : Activé en cas de Défaite
 - ``BT Capturé`` : Activé en cas de Capture

 Pour finir, on peut utiliser l'appel de l'équipe adverse dans le but de faire des conditions :

```ruby
$scene.enemy_party.actors # l'équipe adverse sous forme d'un Array
```
Un exemple est donné juste en-dessous


## Exemple d’un tuto de combat scénarisé (type Prof Chen) : 

![Tuto de combat](https://www.youtube.com/watch?v=aFyLAs7kOWc)

## Exemple d’un combat contre un maître de ligue scénarisé : 

![Tuto Maître de la ligue](https://www.youtube.com/watch?v=QeYMOZOJsy4)




