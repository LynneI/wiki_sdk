# Configurer votre Projet

![tip](warning "Cette page contient du contenu non traduit en Français. Si vous désirez une traduction, merci de contribuer au Wiki (voir bas de la page)")

## Project configuration

PSDK Configuration rely on a project configuration file called `Data/project_identity.yml`. In this file you can specify the maximum level of Pokémon in your game, if you want to used the mouse or not and a lot of other things! This file is in [YAML Format](https://en.wikipedia.org/wiki/YAML) so it's easy to change.

The file `Data/project_identity.yml` is defined by the script [PSDK_CONFIG](https://gitlab.com/pokemonsdk/pokemonsdk/-/blob/development/scripts/00000%20PSDK_CONFIG.rb). This script is the very first PSDK Script to be loaded an propagate your configuration to all other script through the `PSDK_CONFIG` constant. When there's field you don't understand in the `Data/project_identity.yml` file, please refer to the documentation of the `PSDK_CONFIG` script.

### List of field you can configure

Here's a non-exaustive list of fields you can configure:

- `game_title` : a string representing the title of your game (what's shown in the window title)
- `game_version` : a 32bit numbe representing your game version, helping save migration using scripts (this number is saved in the player save).
- `native_resolution` : A string telling the resolution this way : `widthxheight`. Recommanded resolutions are : `320x240` or `640x360`.
- `window_scale` : A number representing the default Window scale. It should always be 2 because it makes the game compatible with most graphic card (giving resolution of 640x480 or 1280x720 depending on your native resolution).  
   ![tip](info "PSDK runs in the native resolution, meaning that if a shader is used the maximum resolution you can get for the viewport / window will be the native resolution. Everything will be pixelated unless PSDK runs in `smooth` mode (which is not recommanded at all)")
- `default_language_code` : Specify the language PSDK uses by default (if there's no choice). Accepted values are `en` `fr` `it` `de` `es` `ko` or `kana`. You can extend the accepted values by adding values to `GameData::Text::Available_Langs`.
- `choosable_language_code` : List of language you can choose before starting the game (or in the option menu). If you don't want language choice, put ` []` after the colon and remove the following line that start by `- `.  
   ![tip](warning "If you empty this list, **you must** remove `:language` from the order in option configuration.")
- `choosable_language_texts` : List of the language name you can choose, it's used by the option menu to let you choose another language during game play.
- `maximum_saves` : Maximum number of saves the player can have. By default it's 4, you can set 1 if you don't want to be cool, more of you want to make a visual novel like Pokemon Game.
- `pokemon_max_level` : Maximum level Pokémon can reach in the game. This is used to calculate the exp tables.
- `mouse_disabled` : If your game doesn't accept mouse interactions. If set to true, it'll make the Mouse module act like if there's no mouse plugged to your computer.
- `mouse_skin` : Name of the file to use as "mouse cursor" in `Graphics/windowskins` (use default one if not set)
- `specific_zoom` : Indicate the zoom to use for character sprites, particles and DynamicLights. If not set, 2 is used as zoom (which is why you don't have to upscale your characters).
- `always_use_form0_for_evolution` : Tell if PSDK will ignore the evolution data from the current Pokémon form and take the one from the default form of the Pokémon.
- `use_form0_when_no_evolution_data` : Tell if PSDK will look into the default form if there's no evolution data found in the current form of the Pokémon.
- `tilemap` : Tilemap configuration, see [Configure Tilemap](#cat_1_2)
- `options` : Option configuration, see [Configure Options](#cat_1_3)
- `layout` : Layout configuration, see [Configure Window, text etc...](#cat_1_4)

### Configure Tilemap

If you use a different screen resolution, you'll probably need to reconfigure the tilemap and [the viewports](#cat_2). Here's all the field you can set in tilemap configuration: 

- `tilemap_class` : Name of the tilemap class to use, use `Yuki::Tilemap16px` if your game runs in `320x240` or `Yuki::Tilemap` if your game runs in `640x360`
- `tilemap_size_x` : Number of tile the tilemap shows on the x axis, you'll need to adjust that value to show more or less tile depending on your game resolution.
- `tilemap_size_y` : Number of tile the tilemap shows on the y axis, you'll need to adjust that value to show more or less tile depending on your game resolution.
- `autotile_idle_frame_count` : Number of frame autotile keeps showing the same animation cell. By default it's 6, this mean Autotiles run at 10FPS in PSDK.
- `center_x` : The center x value for the player sprite, it's a complex RMXP thing calculate this way (approximately) : `(tilemap_size_x - 2) * 32 * 2 - 64`
- `center_y` : The center y value for the player sprite, it's a complex RMXP thing calculate this way (approximately) : `(tilemap_size_y - 2) * 32 * 2 - 64`
- `character_tile_zoom` : Zoom to apply on "tiles" on Events, since PSDK uses `Yuki::Tilemap16px` it's 0.5 (Turning 32x32 tiles into 16x16).
- `maplinker_offset_x` : Old value used to tell how much in x of the east/west map to copy. Now it's used to know the event.x range to copy (excluding the 3 common row).
- `maplinker_offset_y` : Old value used to tell how much in x of the north/south map to copy. Now it's used to know the event.x range to copy (excluding the 3 common row).

### Configure Options

If you want to add specific options this is where you have to set the thing up.

Option configuration consist of two big array :
- `order` telling which order the options should be shown in the option menu (making it easier to remove or sort options). All options are named using a symbol.
- `options` all the options defined as array of array. Here's the structure of an option array :
  * cell 0 is for option identifier (symbol) example : `:message_speed`
  * cell 1 is for option type, it can be `:choice` or `:slider`. You should always use `:choice` unless you know what you're doing.
  * cell 2 is for option values, in `:choice` it's the array of actual values the game uses. In `:slider` it's a Hash containing the following keys : `:increment`, `:min`, `:max`.  
     ![tip](info "You can use a string giving a path to a constant or a method like this : `ModuleName::ClassName::CONSTANT_NAME` or `ModuleName::OtherModule#method_name`. See :message_frame option or :language option.")
  * cell 3 is for option value text, what the UI shows instead of the value used by the script.  
     ![tip](info "You can use a string giving a path to a constant or a method like this : `ModuleName::ClassName::CONSTANT_NAME` or `ModuleName::OtherModule#method_name`. See :message_frame option or :language option.")
  * cell 4 is for the name of the option shown in the UI.   
     ![tip](info "For translation it's highly recommanded to use an array instead of a string. This array contains a symbol as first cell (`:text_get` or `:ext_text`), file_id as second cell and line_index as last cell. This can also be used for the list of value text.")
  * cell 5 is for the description of the option shown in the UI.   
     ![tip](info "For translation it's highly recommanded to use an array instead of a string. This array contains a symbol as first cell (`:text_get` or `:ext_text`), file_id as second cell and line_index as last cell. This can also be used for the list of value text.")
  * cell 6 is for the attribute in `PFM::Option` that will receive the option value.

### Configure Window, text etc...

During the PSDK development, we noticied something really nasty. Some user uses black message Window, some uses white message Window. The problem with those window is that the colors doesn't render the same way so in order to make the UI color consistant (ie you don't edit `graphics/windowskins/_colors.png` for the global text color) we added a layout configuration allowing `Yuki::Message` and `Yuki::ChoiceWindow` to be configured and swap the color depending on the given code.

#### General Layout configuration

Under layout we have a key called `general` that specify the general information about UI layout such as Fonts & size.

Here's the values you can set:
- `supports_pokemon_number` : If the font is basically "Pokémon DS" and has "Pokémon Number" in it (Gen2 way to write numbers, mostly used for HP text or Level text).
- `ttf_files` : A list of Hash describing all the font the game can use. By default PSDK use the font of ID 0 to show all the text. Font of ID 20 is used for tiny text like Pokémon Name in Party Menu. Font of ID 1 is used for bigger text (`\^` in messages). Each entry of ttf_files contains the following keys:
  * `:id` : ID of the font in LiteRGSS (so script can retrieve the font easilly).
  * `:name` : Filename of the font without the `.ttf` in the folder `Fonts` of the project
  * `:size` : Size of the font to use. Be careful this value doesn't always correspond to the size you can get in MS Paint or Photoshop. To find it it's basically trial & error.
  * `:line_height` : Height of the line that PSDK uses to space each line of text (not used a lot but can be usefull for UI Making).
- `alt_sizes` : A list of Hash describing the alternative size you want to use (with `SpriteStack` field `sizeid:` in `add_text`). That's the same as `ttf_file` without the `:name` field.

#### Message Layout configuration

Under layout we have a key called `messages`. It's a Hash of Message configuration. The key `:any` is the fallback in case of the String key corresponding to the classname of the current scene is not found.

Here's all the field you can find in message configuration:
- `windowskin` : name of the windowskin to use (overwrites the Option). Usually set to nil.
- `name_windowskin` : name of the windowskin used to show the name of the speaking NPC. (nil = use the message windowskin)
- `line_count` : Number of line a message show before waiting the player to press ENTER to continue (will show message line after line once the line_count first line has been shown).
- `border_spacing` : Number of pixel used to space message window to each side of the Game Window.
- `default_font` : ID of the font that is used by default (see `ttf_files` above).
- `default_color` : ID of the color that is used to show the message text when `\c[x]` is not specified. This value will be passed into `color_mapping` to get the real color.
- `color_mapping` : Hash associating color_id to x coordinate in `graphics/windowskins/_colors.png`. This can be used to map colors before 10 to color after 10 when the windowskin has a dark background.

#### Choice Layout configuration

Under the layout we have a key called `choices`. It's a bit like `messages` but with less keys since it's about choice window.

Here's all the field you can find in choice configuration:
- `windowskin` : name of the windowskin to use (overwrites the Option). Usually set to nil.
- `border_spacing` : Number of pixel used to space choice window to the message window.
- `default_font` : ID of the font that is used by default (see `ttf_files` above).
- `default_color` : ID of the color that is used to show the choice option text when `\c[x]` is not specified. This value will be passed into `color_mapping` to get the real color.
- `color_mapping` : Hash associating color_id to x coordinate in `graphics/windowskins/_colors.png`. This can be used to map colors before 10 to color after 10 when the windowskin has a dark background.

## La Configuration des Viewports

Si vous retirer le module `Viewport` du script `Config`, PSDK utilisera `Data/Viewport.json` comme source de configuration des viewports.

Le fichier ressemble à ça :
```{
  "main": {
    "x": 0,
    "y": 0,
    "width": 320,
    "height": 240
  }
}```

C'est basiquement un Hash de Hash qui contient toutes les configurations de viewport. Quand vous écrivez :
```ruby
  @viewport = Viewport.create(:main, z)
```
Les paramètres x, y, width & height sont déterminés dans ce fichier. Si vous voulez créer des viewports qui ne font pas l'intégralité de l'écran, vous pouvez utiliser ce fichier de configuration.

![tip](info "`Viewport.create(type, z)` est le seul moyen recommander pour créer un Viewport. Il permet au viewport d'être déplacé quand vous souhaitez ajouter une compatibilité avec le plein écran grâce à `Viewport::GLOBAL_OFFSET_X` & `Viewport::GLOBAL_OFFSET_Y`. N'oubliez pas que PSDK est en 4:3 par défaut et que la plupart des écrans d'ordinateurs sont en 16:9 or 16:10.")

## The file `graphics/windowskins/_colors.png`

This file tells PSDK which color to use when you want to display a text with a specific `color_id`. This file is structured the following way : x coordinate correspond to color_id.   
When y = 0, it's the outline color (when showing outlined text).   
When y = 1, it's the color of the text itself (fill_color).
When y = 2, it's the color of the shadow.

![tip](info "You can disbale the shadow globally by setting the constant `Text::Util::DEFAULT_OUTLINE_SIZE` to 0 instead of nil.")
