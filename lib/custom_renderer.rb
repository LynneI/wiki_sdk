require 'rouge/plugins/redcarpet'

class CustomRenderer < Redcarpet::Render::HTML
  include Rouge::Plugins::Redcarpet

  class SubRenderer < Redcarpet::Render::HTML
    def paragraph(text)
      text
    end
  end
  # Constant containing the Summary title in various langs
  SUMMARY_TITLES = {
    'fr' =>  'Sommaire',
    'en' => 'Contents',
  }
  # @return [String] title of the page
  attr_reader :title

  # Initialize the renderer variables
  def init(lang)
    @categories = {}
    @header_counters = [0, 0, 0, 0]
    @summary_title = SUMMARY_TITLES[@lang = lang] || 'Contents'
    @sub_render ||= Redcarpet::Markdown.new(SubRenderer.new)
  end

  # Finish the document rendering
  def postprocess(full_document)
    <<~FULLDOC
      <!DOCTYPE html>
      <html>
        <head>
          <meta charset="UTF-8">
          <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
          <title>#{@title || 'Article'}</title>
          <!-- {HEAD} -->
        </head>
        <body>
          <!-- {HEADER} -->
          <section class="wiki">
            <!-- {ARTICLE_HEADER} -->
            #{@categories.empty? ? nil : "<nav class=\"article_nav\"><h2>#{@summary_title}</h2>#{generate_nav}</nav>"}
            <article>#{full_document}</article>
            <!-- {ARTICLE_FOOTER} -->
          </section>
          <!-- {FOOTER} -->
        </body>
      </html>
    FULLDOC
  end

  # Generate the nav
  def generate_nav
    last_level = 0
    list_item_closed = true
    str = ''
    @categories.each do |cat, text|
      level = cat.count('_')
      if level < last_level
        str << "</li>\n</ul>\n"
      elsif level > last_level
        str << "\n<ul>"
        list_item_closed = true
      end
      last_level = level
      str << "</li>\n" unless list_item_closed
      str << "<li><a href=\"\##{cat}\">#{text}</a>"
      list_item_closed = false
    end
    str << "</li>\n"
    (last_level - 1).downto(0) do |i|
      str << (i == 0 ? "</ul>\n" : "</ul>\n</li>\n")
    end
    return str
  end

  # Show a block code with the right highlighting
  def block_code(code, language)
    "<pre>\n#{Rouge.highlight(code, language || 'text', 'html')}</pre>\n"
  end

  # Show a block quote
  def block_quote(quote)
    "<blockquote>\n#{quote}</blockquote>\n"
  end

  # Show the foot notes block
  def footnotes(content)
    "<div class=\"footnotes\"><ol>\n#{content}</ol></div>"
  end

  # Show a foot note
  def footnote_def(content, number)
    ref_tag = "<a href=\"#fnref#{number}\">&#8617;</a></p>"
    "<li id=\"fn#{number}\">#{content.rstrip.sub('</p>', ref_tag)}</li>\n"
  end

  # Show a header
  def header(text, header_level)
    ref = create_header_ref(text, header_level)
    "<h#{header_level} id=\"#{ref}\">#{text}</h#{header_level}>\n"
  end

  # Create a new header ref
  def create_header_ref(text, header_level)
    if header_level == 1
      @title = text
      return 'top'
    end
    @header_counters[header_level - 2] += 1
    (header_level - 1).upto(3) { |i| @header_counters[i] = 0 }
    ref = "cat_#{@header_counters[0, header_level - 1].join('_')}"
    @categories[ref] = text
    ref
  end

  # Show a list block
  def list(contents, list_type)
    if list_type == :ordered
      "<ol>\n#{contents}</ol>\n"
    else
      "<ul>\n#{contents}</ul>\n"
    end
  end

  # Show a paragraph
  def paragraph(text)
    "<p>#{text}</p>\n"
  end

  TIP_TRANSLATION = {
    'warning' => '<span>⚠</span>',
    'danger' => '<span>⛔</span>',
    'info' => '<span>ℹ</span>'
  }
  # Show an image
  # ![alt text](https://github.com/adam-p/markdown-here/raw/master/src/common/images/icon48.png "Logo Title Text 1")
  # ![alt text|align](url "figcaption")
  def image(link, title, alt_text)
    if alt_text.include?('|')
      alt_text, align = alt_text.split('|')
      return "<figure class=\"#{align}\"><img src=\"#{link}\" alt=\"#{alt_text}\"/><figcaption>#{title}</figcaption></figure>"
    elsif link.include?('https://www.youtube.com/watch?v=')
      link = link.sub('https://www.youtube.com/watch?v=', 'https://www.youtube.com/embed/')
      return <<~BECAUSEYOUTUBEISWHERETHEPOOPIS
        <div class="center">
          <iframe width="560" height="315" src="#{link}" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
        </div>
      BECAUSEYOUTUBEISWHERETHEPOOPIS
    elsif alt_text.casecmp?('tip')
      return <<~TIP
        <div class="#{link}">
          #{TIP_TRANSLATION[link]}<span>#{@sub_render.render(title)}</span>
        </div>
      TIP
    else
      return "<img src=\"#{link}\" alt=\"#{alt_text}\" title=\"#{title || alt_text}\"/>"
    end
  end
end
